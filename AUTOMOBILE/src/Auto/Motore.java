package Auto;

import java.util.Date;
import java.util.Scanner;
import Auto.Revisione;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Locale;

public class Motore {
	float cilindrata;
	float potenza;
	Revisione revisione;
	boolean revisioneFatta;
	
	void eseguiRevisione() {
		if(!this.revisioneFatta)
			this.revisione = new Revisione();
		else
			this.revisione.eseguiRevisione();
	}

	void visualizza(){
		System.out.println("Potenza: "+this.potenza);
		System.out.println("Cilindrata: " + this.cilindrata);
	}

	String eseguiControlli(){
		String controlli = "";
		while(true){
			System.out.print("inserisci controllo o 0 per terminare: ");
			String scelta = new Scanner(System.in).nextLine();
			if(scelta.equals("0"))
				return controlli;
			controlli += scelta + "</c>";
		}
	}

	void insertRevisione(String r){

		if(r.equals("y") || r.equals("Y")){
			System.out.print("Inserisci la data dell'ultima revisione (DD/MM/AAAA): ");
			String d = new Scanner(System.in).nextLine();
			if(d.split("/").length == 3) {
				Date data = new Date(
					Integer.parseInt(d.split("/")[2]) - 1900,
					Integer.parseInt(d.split("/")[1]) - 1,
					Integer.parseInt(d.split("/")[0])
				);
				if(data.before(Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY).getTime())){
					this.revisione = new Revisione(
						data,
						this.eseguiControlli()
					);
					this.revisioneFatta = true;
					return;
				}
			}
			System.out.println("Data invalida");
			this.revisioneFatta = false;
		}else
			this.revisioneFatta = false;
	}
	
	
	public Motore(){
		System.out.print("Cilindrata: ");
		this.cilindrata = new Scanner(System.in).nextFloat();

		System.out.print("Potenza: ");
		this.potenza = new Scanner(System.in).nextFloat();

		System.out.print("Hai eseguito la revisione? (y / 'any') : ");
		String r = new Scanner(System.in).nextLine();
		this.insertRevisione(r);

	}

	public Motore(
		float cilindrata,
		float potenza,
		Revisione revisione
	){
		this.cilindrata = cilindrata;
		this.potenza = potenza;
		this.revisione = revisione;
		this.revisioneFatta = false;
	}
}
