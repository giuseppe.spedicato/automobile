package Auto;

import Auto.Auto;
import java.util.Scanner;
import java.util.ArrayList;

public class main {

	int getCarId(ArrayList<Auto> automobili){
		if(automobili.size() == 0)
			return -1;
		for (int i = 0; i < automobili.size(); i++)
			System.out.println(i + ") " + automobili.get(i).modello + ", Targa: " + automobili.get(i).targa);
		System.out.print("Inserisci: ");
		return new Scanner(System.in).nextInt();
	}

	int getCarId(ArrayList<Auto> automobili , boolean b){
		int count = 0;
		for (int i = 0; i < automobili.size(); i++){
			if(automobili.get(i).motore.revisioneFatta){
				System.out.println(i + ") " + automobili.get(i).modello + ", Targa: " + automobili.get(i).targa);
				count++;
			}
		}
		if(count == 0) 
			return -1;
		System.out.print("Inserisci: ");
		return new Scanner(System.in).nextInt();
	}

	void systemClear(){
		System.out.print("\033[H\033[2J");
		System.out.flush();
	}

	void systemPause(){
		System.out.print("\nPremi invio per continuare...");
        new Scanner(System.in).nextLine();
	}

	public static void main(String[] args) {
		new main().systemClear();
		
		ArrayList<Auto> automobili = new ArrayList<Auto>();

		while(true){
			int id = 0;
			int scelta;
			System.out.print("----GESTIONE AUTO----\n\n1) aggiungi un auto\n2) visulizza un auto\n3) accendi auto\n4) esegui revisione\n5) visualizza revisione\n0) esci\n\nInserisci: ");
			scelta = new Scanner(System.in).nextInt();
			new main().systemClear();
			if(scelta>=2 && scelta <= 4)
				id = new main().getCarId(automobili);
			else if(scelta == 5)
				id = new main().getCarId(automobili, true);
			if(id >= 0)
				switch(scelta){
					case 1:
						automobili.add(new Auto());
						break;
					case 2:
						automobili.get(id).visualizzaAuto();
						break;
					case 3:
						automobili.get(id).onOff();
						break;
					case 4:
						automobili.get(id).motore.eseguiRevisione();
						break;
					case 5:
						automobili.get(id).motore.revisione.visualizzaControlli();
						break;
					case 0: return;
					default:
						System.out.println("Funzione inesistente");
						break;
				}
			else
				System.out.println("Nessun auto selezionabile");
			id = 0;
			new main().systemPause();
			new main().systemClear();
		}
	}

}
