package Auto;

import java.util.Scanner;

public class InfoProprietario {
	String nome;
	String cognome;
	public InfoProprietario() {
		System.out.print("nome proprietario: ");
		this.nome = new Scanner(System.in).nextLine();

		System.out.print("cognome proprietario: ");
		this.cognome = new Scanner(System.in).nextLine();
	}

	public InfoProprietario(
		String nome,
		String cognome
	){
		this.nome = nome;
		this.cognome = cognome;
	}
	
	void stampaInfo() {
		System.out.println("Nome : " + this.nome + "\nCognome: " + this.cognome);
	}
}
