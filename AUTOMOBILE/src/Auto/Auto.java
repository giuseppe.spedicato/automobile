package Auto;

import java.util.Scanner;
import java.util.ArrayList;
import Auto.InfoProprietario;
import Auto.Motore;

public class Auto {
	String modello;
	String marca;
	float prezzo;
	boolean stato;
	String targa;
	InfoProprietario proprietario;
	Motore motore;
	
	
	void onOff() {
		this.stato = !this.stato;
		System.out.println("Auto" + (this.stato?"accesa":"spenta"));
	}
	
	void visualizzaAuto() {
		System.out.println("modello: "+ this.modello);
		System.out.println("marca: "+ this.marca);
		System.out.println("prezzo: "+ this.prezzo);
		System.out.println("stato: "+ (this.stato? "accesa":"spenta"));
		this.proprietario.stampaInfo();
		this.motore.visualizza();
	}

	
	
	public Auto(){
		System.out.print("modello auto: ");
		this.modello = new Scanner(System.in).nextLine();
		System.out.print("marca auto: ");
		this.marca = new Scanner(System.in).nextLine();
		System.out.print("prezzo auto: ");
		this.prezzo = new Scanner(System.in).nextFloat();
		System.out.print("targa auto: ");
		this.targa = new Scanner(System.in).nextLine();
		this.proprietario = new InfoProprietario();
		this.motore = new Motore();
	}

	public Auto(
		String marca,
		String modello,
		float prezzo,
		boolean stato,
		String targa,
		InfoProprietario proprietario,
		Motore motore
	){
		this.marca = marca;
		this.modello = modello;
		this.prezzo = prezzo;
		this.stato = stato;
		this.targa = targa;
		this.proprietario = proprietario;
		this.motore = motore;
	}
	
}
