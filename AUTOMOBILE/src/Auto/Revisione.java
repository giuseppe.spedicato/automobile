package Auto;

import java.util.Scanner;
import java.util.Date;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Locale;

public class Revisione {
	Date d;
	String controlli;

	// ATTRIBUTO PER PRENDERE LA DATA ATTUALE
	Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);

	public Revisione() {
		this.d = this.calendar.getTime();
		this.controlli = this.eseguiControlli();
		System.out.println("REVISIONE ESEGUITA");
	}

	public Revisione(
		Date d,
		String controlli
	){
		this.d = d;
		this.controlli = controlli;
	}

	void eseguiRevisione(){
		//CONTROLLA SE SONO PASSATI 2 ANNI DALL'ULTIMA REVISIONE
		if((this.d.getYear() >= (this.calendar.getTime().getYear() + 2)) && (this.d.getMonth() >= this.calendar.getTime().getMonth())){
			this.d =this.calendar.getTime();
			this.controlli = this.eseguiControlli();
			System.out.println("REVISIONE ESEGUITA");
		}else
			System.out.println("IMPOSSIBILE ESEGUIRE LA REVISIONE. VECCHIA REVISIONE ANCORA ATTIVA");
	}

	String eseguiControlli(){
		String controlli = "";
		while(true){
			System.out.print("inserisci controllo o 0 per terminare: ");
			String scelta = new Scanner(System.in).nextLine();
			if(scelta.equals("0"))
				return controlli;
			controlli += scelta + "</c>";
		}
	}

	void visualizzaControlli(){
		System.out.println("Data: " + this.d.toLocaleString().split(",")[0]);
		for(String s: this.controlli.split("</c>"))
			System.out.println(s);
	}
}